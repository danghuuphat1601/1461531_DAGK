﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Webgiuaki.Startup))]
namespace Webgiuaki
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
