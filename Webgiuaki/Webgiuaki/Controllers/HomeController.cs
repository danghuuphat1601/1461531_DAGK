﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Webgiuaki.Models.Bus;

namespace Webgiuaki.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index(int page =1)
        {
            return View(SanPhamBus.DanhSach(page ,6));
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult Details(int id)
        {
            return View(SanPhamBus.ChiTiet(id));
        }
    }
}