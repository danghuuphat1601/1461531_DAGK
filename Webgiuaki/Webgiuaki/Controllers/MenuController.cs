﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Webgiuaki.Models.Bus;

namespace Webgiuaki.Controllers
{
    public class MenuController : Controller
    {
        // GET: Menu
        public ActionResult Index(int page = 1)
        {
            var dsSanPham = SanPhamBus.DanhSach(page, 6);
            return View(dsSanPham);
        }

        // GET: Menu/Details/5
        public ActionResult Details(int id)
        {
            return View(MenuBus.ChiTiet(id));
        }
        public ActionResult AllProducts(int id)
        {

            return View(MenuBus.TatCaSanPham(id));
        }

        //// GET: Menu/Create
        //public ActionResult Create()
        //{
        //    return View();
        //}

        //// POST: Menu/Create
        //[HttpPost]
        //public ActionResult Create(FormCollection collection)
        //{
        //    try
        //    {
        //        // TODO: Add insert logic here

        //        return RedirectToAction("Index");
        //    }
        //    catch
        //    {
        //        return View();
        //    }
        //}

        //// GET: Menu/Edit/5
        //public ActionResult Edit(int id)
        //{
        //    return View();
        //}

        //// POST: Menu/Edit/5
        //[HttpPost]
        //public ActionResult Edit(int id, FormCollection collection)
        //{
        //    try
        //    {
        //        // TODO: Add update logic here

        //        return RedirectToAction("Index");
        //    }
        //    catch
        //    {
        //        return View();
        //    }
        //}

        //// GET: Menu/Delete/5
        //public ActionResult Delete(int id)
        //{
        //    return View();
        //}

        //// POST: Menu/Delete/5
        //[HttpPost]
        //public ActionResult Delete(int id, FormCollection collection)
        //{
        //    try
        //    {
        //        // TODO: Add delete logic here

        //        return RedirectToAction("Index");
        //    }
        //    catch
        //    {
        //        return View();
        //    }
        //}
    }
}
