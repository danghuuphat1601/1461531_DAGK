﻿using PetaPoco;
using ShopOnlineConnection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Webgiuaki.Models.Bus
{
    public class SanPhamBus
    {
        public static IEnumerable<SanPham>DanhSach()
        {
            var db = new ShopOnlineConnectionDB();
            return db.Query<SanPham>("select *from SanPham");

        }
        public static Page<SanPham> DanhSach(int page ,int itempage)
        {
            var db = new ShopOnlineConnectionDB();
            return db.Page<SanPham>(page, itempage,"select *from SanPham");

        }

        public static SanPham ChiTiet(int id)
        {
            var sql = new ShopOnlineConnectionDB();
            return sql.SingleOrDefault<SanPham>("select * from SanPham where MaSP = @0", id);
        }
    }
}