﻿using PetaPoco;
using ShopOnlineConnection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Webgiuaki.Models.Bus
{
    public class MenuBus
    {
        public static IEnumerable<LoaiSanPham> DanhSach()
        {
            var sql = new ShopOnlineConnectionDB();
            return sql.Query<LoaiSanPham>("select * from LoaiSanPham ");
        }

        public static Page<SanPham> DanhSach(int page, int itempage)
        {
            var sql = new ShopOnlineConnectionDB();
            return sql.Page<SanPham>(page, itempage, "select * from SanPham ");
        }

        public static SanPham ChiTiet(int id)
        {
            var sql = new ShopOnlineConnectionDB();
            return sql.SingleOrDefault<SanPham>("select * from SanPham where MaSP = @0", id);
        }

        public static IEnumerable<SanPham> TatCaSanPham(int id)
        {
            var sql = new ShopOnlineConnectionDB();
            return sql.Query<SanPham>("select * from SanPham where MaLoaiSanPham = @0", id).ToList();
        }
    }
}